import { Component } from '@angular/core';
import { HttpService } from './models/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'app';

  constructor( private httpService: HttpService ) {
  }

}
